

from django.db.models import fields
from lab_1.models import Friend
from django.forms import ModelForm
from lab_1.models import Friend

class FriendForm(ModelForm):
    class Meta:
        model = Friend    
        fields = "__all__"