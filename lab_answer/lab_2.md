1. Apakah perbedaan antara JSON dan XML?
                |           JSON                |                XML                  |
________________|_______________________________|_____________________________________|              
Tipe            |JSON merupakan format data     |XML merupakan markup language        |
________________|_______________________________|_____________________________________|
Orientasi       |JSON berorientasi pada data    |XML berorientasi kepada dokumen      |
________________|_______________________________|_____________________________________|
Kompleksitas    |Sintaks sederhana dan mudah    |Sintaks lebih rumit dalam dan sedikit|
                |dipahami                       |susah dipahami                       |
________________|_______________________________|_____________________________________|
Namespace       |Tidak mendukung dalam          |Mendukung pemakaian namespaces,      |
                |pemakaian namespaces,          |komentar dan juga metadata           |
                |komentar atau menulis metadata |                                     |
________________|_______________________________|_____________________________________|    
Ekstensi        |Memiliki ekstensi .json        |Memiliki ekstensi .xml               |
________________|_______________________________|_____________________________________|
Array           |Mendukung Array yang dapat     |Tidak mendukung adanya pemakaian     |
                |diakses                        |array                                |
________________|_______________________________|_____________________________________|
Kecepatan       |Sangat cepat karena memiliki   |Lebih lambat dibandingkan JSON       |
                |file yang ringan               |                                     |
________________|_______________________________|_____________________________________|
Tipe Data       |Hanya mendukung tipe data      |Mendukung banyak tipe data           |
                |string, boolean, number, array,|string, number, gambar, grafik,      |
                |dan object                     |charts, dan lain-lain                |
________________|_______________________________|_____________________________________|
Dapat disimpulkan bahwa JSON merupakan format data yang lebih ringan, sederhana, dan mudah
dibaca. Sedangkan XML adalah bahasa markup yang lebih rumit, namun lebih extensible. 

2. Apakah perbedaan antara HTML dan XML?
                |           HTML                |                XML                  |
________________|_______________________________|_____________________________________|              
Fokus           |HTML berfokus pada penampilan  |XML berfokus pada transfer data      |
                |data                           |                                     |
________________|_______________________________|_____________________________________|
Tag Penutup     |Tag penutup dapat diberikan    |Wajib menggunakan tag penutup dan    |
                |maupun tidak dan sudah diatur  |dapat didefinisikan sendiri          |
________________|_______________________________|_____________________________________|
Tipe Penulisan  |HTML bersifat case insensitive |XML bersifat case sensitive          |
________________|_______________________________|_____________________________________|    
Ekstensi        |Memiliki ekstensi .html        |Memiliki ekstensi .xml               |
________________|_______________________________|_____________________________________|
Struktural      |Tidak mengandung informasi     |Informasi pada XML disediakan        |
                |struktural                     |                                     |
________________|_______________________________|_____________________________________|
Namespace       |Tidak mendukung dalam          |Mendukung pemakaian namespaces,      |
                |pemakaian namespaces           |komentar dan juga metadata           |
                |komentar atau menulis metadata |                                     |
________________|_______________________________|_____________________________________|  
Dapat disimpulkan bahwa meskipun HTML dan XML sama-sama merupakan bahasa markup, namun
keduanya memiliki perbedaan yaitu HTML mempunyai fokus pada bagaimana data ditampilkan
sedangkan XML berfokus pada transfer dari data dan juga konteksnya. HTML dan XML dapat
digunakan bersama-sama agar dapat saling melengkapi fungsinya masing-masing.

Referensi:
- Hasil diskusi minggu 4 Kelompok B06
- https://id.wikibooks.org/wiki/Pemrograman_XML/XML_vs_HTML
- https://blogs.masterweb.com/perbedaan-xml-dan-html/
- https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html