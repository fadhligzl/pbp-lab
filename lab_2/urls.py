from django.urls import path
from .views import index, xml, json
from lab_2 import views

urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json')
]
