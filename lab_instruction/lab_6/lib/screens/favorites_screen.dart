import 'package:flutter/material.dart';

import '../models/meal.dart';
import '../widgets/meal_item.dart';

class FavoritesScreen extends StatelessWidget {
  // final List<Meal> favoriteMeals;

  FavoritesScreen();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width / 1.05,
        child: Card(
          margin: const EdgeInsets.all(2.0),
          elevation: 3.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "Mitos atau Fakta?",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              const Divider(
                color: Colors.black,
                indent: 10,
                endIndent: 10,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Text(
                  "Quiz ini akan membahas mengenai mitos dan fakta seputar Covid-19 yang beredar pada masyarakat.",
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 15),
                  width: MediaQuery.of(context).size.width / 1.4,
                  child: new Image.asset(
                      'assets/images/mitosfaktacovidimg400.jpg')),
              const SizedBox(height: 10),
              ElevatedButton(
                style: ButtonStyle(),
                onPressed: () {},
                child: const Text('Start Quiz'),
              ),
              Text(
                "atau",
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 3),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: ElevatedButton(
                  style: ButtonStyle(),
                  onPressed: () {},
                  child: const Text('Lihat Leaderboard'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    // if (favoriteMeals.isEmpty) {
    //   return Center(
    //     child: Text('Belum ada yang dipilih.'),
    //   );
    // } else {
    //   return ListView.builder(
    //     itemBuilder: (ctx, index) {
    //       return MealItem(
    //         id: favoriteMeals[index].id,
    //         title: favoriteMeals[index].title,
    //         imageUrl: favoriteMeals[index].imageUrl,
    //         duration: favoriteMeals[index].duration,
    //         affordability: favoriteMeals[index].affordability,
    //         complexity: favoriteMeals[index].complexity,
    //       );
    //     },
    //     itemCount: favoriteMeals.length,
    //   );
    // }
  }
}
