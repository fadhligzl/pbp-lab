import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 110,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.center,
            // color: Theme.of(context).accentColor,
            color: Colors.green,
            child: Text(
              'ZONAHIJAU',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  // color: Theme.of(context).primaryColor),
                  color: Colors.white),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          buildListTile('Mitos / Fakta?', Icons.query_stats, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('FUFU', Icons.question_answer_outlined, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('List RS', Icons.local_hospital_outlined, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('FaQ', Icons.quiz, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Infografis', Icons.gradient_sharp, () {
            Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
        ],
      ),
    );
  }
}
