import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Mitos atau Fakta? | Form",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[300],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),
        )),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ZonaHijau"),
        backgroundColor: Colors.teal[300],
      ),
      body: Center(
          child: Container(
        width: MediaQuery.of(context).size.width / 1.25,
        child: Card(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Tambah Pertanyaan Quiz",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Masukkan pertanyaan",
                          labelText: "Pertanyaan",
                          icon: Icon(Icons.help_outline),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Pertanyaan tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Mitos/Fakta",
                          labelText: "Jawaban",
                          icon: Icon(Icons.question_answer_outlined),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Jawaban tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Masukkan pembahasan",
                          labelText: "Alasan",
                          icon: Icon(Icons.grading),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Alasan tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Masukkan nomor soal",
                          labelText: "Nomor",
                          icon: Icon(Icons.format_list_numbered_sharp),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Nomor tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    RaisedButton(
                      child: Text(
                        "Tambah",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.teal[300],
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Pertanyaan baru berhasil ditambah!");
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      )),
    );
  }
}
